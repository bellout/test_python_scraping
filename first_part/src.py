import random as rd 
################the first part python test#################
################   exercise one   #################
print("exercice 1")
def exercise_one():
 pass
#parcourir les nombres de 0 jusq'au 100et afficher par la suite les multiples de 3 et 5
for i in range(100):
    if i % 3 == 0 and i % 5 == 0: #si i est un multiple de 3 et de 5 à la fois on affiche threefive
        print("threefive")
        continue
    elif i % 3 == 0:  #s'il est un multiple de 3 on affiche three 
        print("three")
        continue
    elif i % 5 == 0: #s'il est un multiple de 5 on affiche five 
        print("five")
        continue
    print(i)         
    
################   find the missing number   #################
# find the missing number in an array

# find_missing_nb takes list (A) as argument
def find_missing_nb(A):   # A : the array that the user will choose 
    pass
    n = len(A)
    total = (n + 1)*(n + 2)/2
    sum_of_A = sum(A)
    return total - sum_of_A
print("exercice 2")
# Driver program to test the above function
A = [1, 2, 4, 5, 6]
print(A)
miss = find_missing_nb(A)

print("the missing number in the array A is :", miss)
# This code is contributed by Pratik Chhajer

################   function calculate   #################
# Python program to find sum of list with different 
# types. 

def calculate(l): 

	# returning sum of list using List comprehension 
	return sum([int(i) for i in l if type(i)== int]) 

# Declaring list 
list1 = [1, 'latifa', 2,'bellout', 'stage', 10] 
list2 = [20,15,'eva','master2'] 

# Result of sum of list 
sum1=calculate(list1) 
sum2=calculate(list2) 
print("exercice 3")
print("la somme des nombres entiers de la premiere liste est :",sum1)
print("la somme des nombres entiers de la deuxieme liste est :",sum2)
########### function sort without changing the position on negtive numbers ###########
print("exercice 4")

# Function to sort the array such that 
# negative values do not get affected 
def sortArray(A, n): 
 
	# Store all non-negative values 
	ans=[] 
	for i in range(n): 
		if (A[i] >= 0): 
			ans.append(A[i]) 
 
	# Sort non-negative values 
	ans = sorted(ans) 
 
	j = 0
	for i in range(n): 
 
		# If current element is non-negative then 
		# update it such that all the 
		# non-negative values are sorted 
		if (A[i] >= 0): 
			A[i] = ans[j] 
			j += 1
 
	# Print the sorted array 
	for i in range(n): 
		print(A[i],end = " ") 
 
# Driver code 
 
arr = [2, -6, -3, 8, 4, 1] 
 
n = len(arr) 
 
sortArray(arr, n) 


